##Overview
Android app for selecting a movie to watch amongst a group of friends. 

##Screenshots
![ScreenShot](https://bitbucket.org/jordan_steele/your-movie-night/raw/a6f4a22794960422b69cca348e7603ec446fc146/screenshots/Screenshot_2013-11-24-22-28-50.png)
![ScreenShot](https://bitbucket.org/jordan_steele/your-movie-night/raw/a6f4a22794960422b69cca348e7603ec446fc146/screenshots/Screenshot_2013-11-24-22-28-59.png)
![ScreenShot](https://bitbucket.org/jordan_steele/your-movie-night/raw/0b2d46108aa079725c07cdb0752479e79d1087b3/screenshots/Screenshot_2013-11-25-23-43-04.png)
