package jordan.steele.ourmovienight.helpers;

public class Movie {
	private String title;
	private String releaseDate;
	private String rating;
	private String runtime;
	private String criticsScore;
	
	public Movie(String title, String releaseDate, String rating, String runtime, String criticsScore) {
		this.title = title;
		this.releaseDate = releaseDate;
		this.rating = rating;
		this.runtime = runtime;
		this.criticsScore = criticsScore;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public String getCriticsScore() {
		return criticsScore;
	}

	public void setCriticsScore(String criticsScore) {
		this.criticsScore = criticsScore;
	}
	
	
}
