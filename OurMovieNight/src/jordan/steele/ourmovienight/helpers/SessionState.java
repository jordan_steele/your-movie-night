package jordan.steele.ourmovienight.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.util.Log;

public class SessionState {
	private static int numberOfParticipants;
	private static ArrayList<Movie> nominatedMovies;
	private static Map<Movie, Integer> movieVotes;
	private static int votesMade = 0;
	
	public static void resetState() {
		numberOfParticipants = -1;
		nominatedMovies = null;
		movieVotes = null;
		votesMade = 0;
	}
	
	public static int getNumberOfParticipants() {
		return numberOfParticipants;
	}
	public static void setNumberOfParticipants(int numberOfParticipants) {
		SessionState.numberOfParticipants = numberOfParticipants;
	}
	public static ArrayList<Movie> getNominatedMovies() {
		return nominatedMovies;
	}
	public static void addNominatedMovie(Movie movie) {
		if (nominatedMovies == null){
			nominatedMovies = new ArrayList<Movie>();
		}
		SessionState.nominatedMovies.add(movie);
	}
	public static Map<Movie, Integer> getMovieVotes() {
		return movieVotes;
	}
	public static void makeMovieVote(int points, Movie movie) {
		int pointsAfter = points;
		
		if (movieVotes == null){
			movieVotes = new HashMap<Movie, Integer>();
		}
		
		if (movieVotes.get(movie) != null){
			pointsAfter += movieVotes.get(movie);
		}
		movieVotes.put(movie, pointsAfter);
	}
	public static Movie getWinner(){	
		Log.w("SWAG", movieVotes.toString());
		Movie currentWinner = null;
		int currentWinnerScore = 10000;
		int currentScore;
	
		for (Movie movie : movieVotes.keySet()){
			currentScore = movieVotes.get(movie);
			if (currentScore < currentWinnerScore){
				currentWinnerScore = currentScore;
				currentWinner = movie;
			}
		}
		
		return currentWinner;
	}
	public static int getVotesMade() {
		return votesMade;
	}
	public static void incrementVotesMade() {
		votesMade++;
	}
}
