package jordan.steele.ourmovienight.activities;

import jordan.steele.ourmovienight.R;
import jordan.steele.ourmovienight.helpers.SessionState;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ParticipantsActivity extends BaseActivity {

	private TextView participantsHelpText;
	private TextView participantsText;
	private Button doneButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_participants);

		// Show the Up button in the action bar.
		super.setupActionBar();

		//Get activity views
		participantsHelpText = (TextView) findViewById(R.id.participants_help_text);
		participantsText = (TextView) findViewById(R.id.participants_text);
		doneButton = (Button) findViewById(R.id.participants_done_button);

		//Get custom type face   
		Typeface latoLight = Typeface.createFromAsset(getAssets(),"fonts/Lato-Lig.ttf");
		Typeface latoLightItalic = Typeface.createFromAsset(getAssets(),"fonts/Lato-LigIta.ttf");

		//Apply typefaces
		participantsHelpText.setTypeface(latoLightItalic);
		participantsText.setTypeface(latoLight);
		doneButton.setTypeface(latoLight);
	}

	/**
	 * Called when the user presses the button to decrease the amount
	 * of participants
	 * @param view
	 */
	public void decreaseParticipants(View view) {
		int currentParticipants = Integer.parseInt((String) participantsText.getText());
		int newParticipants;

		switch (currentParticipants) {
		case 2: 
			newParticipants = 10;
			break;
		default:
			newParticipants = currentParticipants - 1;
			break;
		}

		participantsText.setText("" + newParticipants);
	}

	/**
	 * Called when the user presses the button to increase the amount
	 * of participants
	 * @param view
	 */
	public void increaseParticipants(View view) {
		int currentParticipants = Integer.parseInt((String) participantsText.getText());
		int newParticipants;

		switch (currentParticipants) {
		case 10: 
			newParticipants = 2;
			break;
		default:
			newParticipants = currentParticipants + 1;
			break;
		}

		participantsText.setText("" + newParticipants);
	}

	/**
	 * Called when the user has pressed the button to move on to the next screen
	 * @param view
	 */
	public void doneParticipants(View view) {
		//Save selection for number of participants
		SessionState.setNumberOfParticipants(Integer.parseInt((String)participantsText.getText()));

		//Start next activity
		Intent intent = new Intent(this, NominationsActivity.class);
		startActivity(intent);
	}
}
