package jordan.steele.ourmovienight.activities;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import jordan.steele.ourmovienight.R;
import jordan.steele.ourmovienight.adapters.SearchResultsAdapter;
import jordan.steele.ourmovienight.helpers.Movie;
import jordan.steele.ourmovienight.helpers.SessionState;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

public class NominationsActivity extends BaseActivity {

	private Context context;
	private TextView nominationsHelp;
	private ListView searchResultsListView;
	private Button searchGoButton;
	private EditText searchTitle;
	private Movie selectedMovie;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_nominations);
		
		this.context = this;
		
		//Get views
		nominationsHelp = (TextView) findViewById(R.id.nominations_help_text);
		searchResultsListView = (ListView) findViewById(R.id.search_results_list_view);
		searchGoButton = (Button) findViewById(R.id.search_go_button);
		searchTitle = (EditText) findViewById(R.id.search_title_edit_text);
		
		//Get custom type face   
		Typeface latoReg = Typeface.createFromAsset(getAssets(),"fonts/Lato-Reg.ttf");
		Typeface latoLight = Typeface.createFromAsset(getAssets(),"fonts/Lato-Lig.ttf");
		Typeface latoLightItalic = Typeface.createFromAsset(getAssets(),"fonts/Lato-LigIta.ttf");
		
		//Apply typefaces
		nominationsHelp.setTypeface(latoLightItalic);
		searchTitle.setTypeface(latoLight);
		searchGoButton.setTypeface(latoReg);
	}
	
	public void search(View view){
		//Hides the keyboard
		InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE); 

		inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                   InputMethodManager.HIDE_NOT_ALWAYS);
		
		//Makes asynch call to get search results
		new JSONCall().execute(searchTitle.getText().toString());
	}
	
	private class JSONCall extends AsyncTask<String, Void, Void> {

		private ArrayList<Movie> searchResults = new ArrayList<Movie>();
		private ProgressDialog dialog = new ProgressDialog(NominationsActivity.this);
		
		@Override
		protected void onPreExecute() {
			this.dialog.setMessage("Please wait");
			this.dialog.show();
		}

		@Override
		protected Void doInBackground(String... arg0) {
			String searchResult = getSearchResult(arg0[0]);
			try {
				searchResults.clear();
				JSONObject jsonObject = new JSONObject(searchResult);
				JSONArray jsonMoviesArray = new JSONArray(jsonObject.getString("movies"));
				for(int i = 0; i < jsonMoviesArray.length(); i++){
					JSONObject jsonMovieObject = jsonMoviesArray.getJSONObject(i);
					searchResults.add(new Movie(jsonMovieObject.getString("title"),
													   jsonMovieObject.getString("year"),
													   jsonMovieObject.getString("mpaa_rating"),
													   jsonMovieObject.getString("runtime"),
													   jsonMovieObject.getJSONObject("ratings").getString("critics_score")));
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void unused) {
			if (dialog.isShowing()) {
			    dialog.dismiss();
			}

			searchResultsListView.setAdapter(new SearchResultsAdapter(context, searchResults));
			
			//Assign action for clicking on contact in grid
			searchResultsListView.setOnItemClickListener(new OnItemClickListener() {
	            @Override
				public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
//	            	selectedMovie = new Movie((String)((TextView) v.findViewById(R.id.search_result_title)).getText(),
//	            							  (String)((TextView) v.findViewById(R.id.search_result_release_date)).getText(),
//	            							  (String)((TextView) v.findViewById(R.id.search_result_synopsis)).getText());
	            	
	            	selectedMovie = (Movie) v.getTag();
	            	
	            	new AlertDialog.Builder(context)
	                .setIcon(android.R.drawable.ic_dialog_alert)
	                .setTitle(R.string.nomination_confirmation_title)
	                .setMessage(getResources().getString(R.string.nomination_confirmation_messaage) 
	                			+ " '" + selectedMovie.getTitle() + "'")
	                .setPositiveButton(R.string.nomination_confirmation_poss, new DialogInterface.OnClickListener() {
	                    @Override
	                    public void onClick(DialogInterface dialog, int which) {
	                    	//Add selected movie to nominated list
	                    	SessionState.addNominatedMovie(selectedMovie);
	                    	
	                    	if (SessionState.getNominatedMovies().size() < SessionState.getNumberOfParticipants()){
	                    		Intent intent = new Intent(context, PassPhoneActivity.class);
	                    		startActivity(intent);
	                    	}
	                    	else {
	                    		Intent intent = new Intent(context, VotingActivity.class);
	                    		startActivity(intent);
	                    	}
	                    }
	                })
	                .setNegativeButton(R.string.nomination_confirmation_neg, null)
	                .show();
	            }
	        });
		}
	}

	public String getSearchResult(String title) {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		String queryString = title.replace(' ', '+');
		HttpGet httpGet = new HttpGet("http://api.rottentomatoes.com/api/public/v1.0/movies.json?apikey=a5wwg5emy9exxp98rd3rb4c3&q=" +
									  queryString +
									  "&page_limit=10");
		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e(NominationsActivity.class.toString(), "Failed to download file");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return builder.toString();
	}



}
