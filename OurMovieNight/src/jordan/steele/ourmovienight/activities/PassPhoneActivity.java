package jordan.steele.ourmovienight.activities;

import jordan.steele.ourmovienight.R;
import jordan.steele.ourmovienight.helpers.SessionState;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class PassPhoneActivity extends BaseActivity {

	private TextView passPhoneText;
	private Button passPhoneButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pass_phone);
		
		//Get views
		passPhoneText = (TextView) findViewById(R.id.pass_phone_text);
		passPhoneButton = (Button) findViewById(R.id.pass_phone_button);
		
		//Get custom type face   
		Typeface latoLight = Typeface.createFromAsset(getAssets(),"fonts/Lato-Lig.ttf");
		Typeface latoLightItalic = Typeface.createFromAsset(getAssets(),"fonts/Lato-LigIta.ttf");
		
		//Apply typefaces
		passPhoneText.setTypeface(latoLightItalic);
		passPhoneButton.setTypeface(latoLight);
	}

	public void passPhoneDone(View view) {
		if (SessionState.getNominatedMovies().size() < SessionState.getNumberOfParticipants()){
			Intent intent = new Intent(this, NominationsActivity.class);
			startActivity(intent);
		}
		else {
			Intent intent = new Intent(this, VotingActivity.class);
			startActivity(intent);
		}
	}

}
