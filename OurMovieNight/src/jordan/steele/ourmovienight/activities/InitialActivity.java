package jordan.steele.ourmovienight.activities;

import jordan.steele.ourmovienight.R;
import jordan.steele.ourmovienight.helpers.SessionState;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class InitialActivity extends BaseActivity {

	private TextView helpText;
	private Button startButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_initial);

		//Get Views
		helpText = (TextView) findViewById(R.id.initial_help_text);
		startButton = (Button) findViewById(R.id.start_button);

		//Get custom type face   
		Typeface latoLight = Typeface.createFromAsset(getAssets(),"fonts/Lato-Lig.ttf");
		Typeface latoLightItalic = Typeface.createFromAsset(getAssets(),"fonts/Lato-LigIta.ttf");
		
		//Apply typefaces
		helpText.setTypeface(latoLightItalic);
		startButton.setTypeface(latoLight);
	}

	public void startSelection(View view) {
		Intent intent = new Intent(this, ParticipantsActivity.class);
		startActivity(intent);
	}

}
