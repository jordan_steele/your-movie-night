package jordan.steele.ourmovienight.activities;

import jordan.steele.ourmovienight.R;
import jordan.steele.ourmovienight.helpers.SessionState;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class ResultsActivity extends BaseActivity {

	private TextView preludeText;
	private TextView winnerText;
	private Button restartButton; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_results);
		
		//Get views
		preludeText = (TextView) findViewById(R.id.winner_prelude_text);
		winnerText = (TextView) findViewById(R.id.winner_movie_text);
		restartButton = (Button) findViewById(R.id.restart_button);
		
		//Get custom type faces
		Typeface latoLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Lig.ttf");
		Typeface latoLightItalic = Typeface.createFromAsset(getAssets(),"fonts/Lato-LigIta.ttf");
		
		//Apply typefaces
		preludeText.setTypeface(latoLightItalic);
		winnerText.setTypeface(latoLight);
		restartButton.setTypeface(latoLight);
		
		winnerText.setText(SessionState.getWinner().getTitle());
	}
}
