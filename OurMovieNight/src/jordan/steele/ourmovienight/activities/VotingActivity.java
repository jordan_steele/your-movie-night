package jordan.steele.ourmovienight.activities;

import java.util.ArrayList;

import jordan.steele.ourmovienight.R;
import jordan.steele.ourmovienight.helpers.Movie;
import jordan.steele.ourmovienight.helpers.SessionState;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class VotingActivity extends BaseActivity {

	private RelativeLayout votingContainer;
	private Button doneButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_voting);
		
		//Get views
		votingContainer = (RelativeLayout) findViewById(R.id.voting_container);
		doneButton = (Button) findViewById(R.id.voting_done_button);
		
		//Get custom type faces
		Typeface latoLight = Typeface.createFromAsset(getAssets(), "fonts/Lato-Lig.ttf");
		
		//Apply typefaces
		doneButton.setTypeface(latoLight);
		
		//Get rank colours
		ArrayList<Integer> rankColours = this.getRankColours();
		
		View previousMovieView = null;
		for(int i = 0; i < SessionState.getNominatedMovies().size(); i++){
			//Inflate view from xml
			LayoutInflater mInflater = (LayoutInflater) this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);			
			View newMovieViewContainer = mInflater.inflate(R.layout.vote_movie_option, null);
			
			//Set text to title of current movie and add movie as a tag to the view
			TextView newMovieView = (TextView) newMovieViewContainer.findViewById(R.id.vote_movie_title);		
			Movie newMovie = SessionState.getNominatedMovies().get(i);
			System.out.println(newMovie.getTitle());
			if (newMovie.getTitle().length() > 20) {
				newMovieView.setText(newMovie.getTitle().substring(0, 20) + "...");
			} else {
				newMovieView.setText(newMovie.getTitle());
			}
			newMovieView.setTag(newMovie);
			
			//Set colour and text of the current movie rank
			TextView newRankView = (TextView) newMovieViewContainer.findViewById(R.id.vote_movie_rank);
			newRankView.setText(Integer.valueOf(i + 1).toString());
			newRankView.setBackgroundColor(rankColours.get((i) % rankColours.size()));
			
			//Apply typeface
			newMovieView.setTypeface(latoLight);
			
			//Set up dragging
			newMovieView.setOnTouchListener(new MyTouchListener());
			newMovieViewContainer.setOnDragListener(new MyDragListener());

			//Set below previous view
			RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
			        ViewGroup.LayoutParams.WRAP_CONTENT);

			if (previousMovieView != null){
				p.addRule(RelativeLayout.BELOW, previousMovieView.getId());
			}
			newMovieViewContainer.setLayoutParams(p);
			
			//Add view to activity
			votingContainer.addView(newMovieViewContainer);
			
			//Update the previous view
			previousMovieView = newMovieViewContainer;
			previousMovieView.setId(300+i);
		}
	}
	
	public void votingDone(View view) {
		//Update session with the selected votes		
		int nextVotes = 1;
		for(int i = 0; i < votingContainer.getChildCount(); i++){
			View currentMovieContainer = votingContainer.getChildAt(i);
			TextView currentMovieTitle = (TextView) currentMovieContainer.findViewById(R.id.vote_movie_title);
			if (currentMovieTitle != null){
				Movie currentMovie = (Movie) currentMovieTitle.getTag();
				SessionState.makeMovieVote(nextVotes, currentMovie);
				nextVotes++;
			}
		}
		
		//Update the number of people who have voted
		SessionState.incrementVotesMade();
		
		//Move to the pass phone activity 
		//or the results activity if all voting is completed
		if (SessionState.getVotesMade() < SessionState.getNumberOfParticipants()){
			Intent intent = new Intent(this, PassPhoneActivity.class);
    		startActivity(intent);
		}
		else {
			Intent intent = new Intent(this, ResultsActivity.class);
			startActivity(intent);
		}
	}
	
	private ArrayList<Integer> getRankColours() {
		ArrayList<Integer> rankColours = new ArrayList<Integer>();
		rankColours.add(getResources().getColor(R.color.peter_river));
		rankColours.add(getResources().getColor(R.color.emerald));
//		rankColours.add(getResources().getColor(R.color.carrot));
		rankColours.add(getResources().getColor(R.color.sun_flower));
		rankColours.add(getResources().getColor(R.color.amethyst));
		rankColours.add(getResources().getColor(R.color.wet_asphalt));
		return rankColours;
	}

	private final class MyTouchListener implements OnTouchListener {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
				ClipData data = ClipData.newPlainText("", "");
				DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
				view.startDrag(data, shadowBuilder, view, 0);
				view.setVisibility(View.INVISIBLE);
				return true;
			} else {
				return false;
			}
		}
	}

	class MyDragListener implements OnDragListener {
		@Override
		public boolean onDrag(View v, DragEvent event) {
			int action = event.getAction();
			switch (action) {
			case DragEvent.ACTION_DRAG_STARTED:
				// do nothing
				break;
			case DragEvent.ACTION_DRAG_ENTERED:
				break;
			case DragEvent.ACTION_DRAG_EXITED:
				break;
			case DragEvent.ACTION_DROP:
				// Dropped, reassign View to ViewGroup
				View draggedView = (View) event.getLocalState();
				ViewGroup oldParent = (ViewGroup) draggedView.getParent();
				oldParent.removeView(draggedView);		
				LinearLayout newParent = (LinearLayout) v;
				newParent.addView(draggedView);
				
				View swappedView = newParent.findViewById(R.id.vote_movie_title);
				newParent.removeView(swappedView);
				oldParent.addView(swappedView);
				draggedView.setVisibility(View.VISIBLE);
				break;
			case DragEvent.ACTION_DRAG_ENDED:
				View viewDraggedFail = (View) event.getLocalState();
				viewDraggedFail.setVisibility(View.VISIBLE);
				break;
			default:
				break;
			}
			return true;
		}
	}

}
