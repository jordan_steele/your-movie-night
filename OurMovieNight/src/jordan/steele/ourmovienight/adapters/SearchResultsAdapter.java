package jordan.steele.ourmovienight.adapters;


import java.util.ArrayList;

import jordan.steele.ourmovienight.R;
import jordan.steele.ourmovienight.helpers.Movie;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SearchResultsAdapter extends BaseAdapter {

	private Context context;
	private ArrayList<Movie> searchResults;

	public SearchResultsAdapter(Context context, ArrayList<Movie> searchResults) {
		this.context = context;
		this.searchResults = searchResults;
	}

	@Override
	public int getCount() {
		return searchResults.size();
	}

	@Override
	public Object getItem(int position) {
		return searchResults.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {  // if it's not recycled, initialize some attributes
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			
			convertView = mInflater.inflate(R.layout.search_result_item, null);
		}
		
		//Get Views
		TextView titleView = (TextView) convertView.findViewById(R.id.search_result_title);
		TextView releaseDateView = (TextView) convertView.findViewById(R.id.search_result_release_date);
		TextView ratingView = (TextView) convertView.findViewById(R.id.search_result_rating);
		TextView runtimeView = (TextView) convertView.findViewById(R.id.search_result_runtime);
		View criticScoreBar = convertView.findViewById(R.id.search_result_critic_score_bar);
		View criticScoreBarTop = convertView.findViewById(R.id.search_result_critic_score_bar_top);
		View criticScoreBarBottom = convertView.findViewById(R.id.search_result_critic_score_bar_bottom);
		View criticScoreBarBlank = convertView.findViewById(R.id.search_result_critic_score_bar_blank);
		//TextView criticScoreView = (TextView) convertView.findViewById(R.id.search_result_critic_score);

		//Get current movie
		Movie currentMovie = searchResults.get(position);
		
		//Set up text views based on movie
		titleView.setText(currentMovie.getTitle());
		releaseDateView.setText("Year: " + currentMovie.getReleaseDate());
		ratingView.setText("Rating: " + currentMovie.getRating());
		runtimeView.setText("Runtime: " + currentMovie.getRuntime() + "mins");
		
		//Set up critic score bar widths
		float criticScore = Integer.parseInt(currentMovie.getCriticsScore()) / 100f;

		
		criticScoreBar.setLayoutParams(new LinearLayout.LayoutParams(0, 
																	 20,
																	 criticScore));
		criticScoreBarBlank.setLayoutParams(new LinearLayout.LayoutParams(0, 
				 													 20,
				 													 1-criticScore));
		
		//Set up critic score bar colours
		int colourTop;
		int colourBottom;
		if (criticScore < 0.4){
			colourTop = R.color.carrot;
			colourBottom = R.color.pumpkin;
		}
		else if (criticScore < 0.75) {
			colourTop = R.color.sun_flower;
			colourBottom = R.color.orange;
		}
		else {
			colourTop = R.color.emerald;
			colourBottom = R.color.nephritis;
		}
		criticScoreBarTop.setBackgroundColor(context.getResources().getColor(colourTop));
		criticScoreBarBottom.setBackgroundColor(context.getResources().getColor(colourBottom));
		
		
		//Get custom type face   
		Typeface latoLight = Typeface.createFromAsset(context.getAssets(),"fonts/Lato-Lig.ttf");
		Typeface latoLightItalic = Typeface.createFromAsset(context.getAssets(),"fonts/Lato-LigIta.ttf");
		
		//Apply custom typeface
		titleView.setTypeface(latoLightItalic);
		releaseDateView.setTypeface(latoLight);
		ratingView.setTypeface(latoLight);
		runtimeView.setTypeface(latoLight);
		
		//Save movie to the container view for later use
		convertView.setTag(currentMovie);
		
		return convertView;
	}

}
